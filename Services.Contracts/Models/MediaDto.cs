﻿using Model.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Contracts.Models
{
    public class MediaDto
    {
        public int Id { get; set; }

        public string FirstFileName { get; set; }
        
        public Guid SecondFileName { get; set; }
        
        public MediaType Type { get; set; }
    }
}
