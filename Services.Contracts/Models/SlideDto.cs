﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Contracts.Models
{
    public class SlideDto
    {
        public int Id { get; set; }

        public int Number { get; set; }

        public MediaDto Media { get; set; }

        public TimeSpan Duration { get; set; }
    }
}
