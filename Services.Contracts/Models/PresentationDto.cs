﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Contracts.Models
{
    public class PresentationDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public IEnumerable<SlideDto> Slides { get; set; }
    }
}
