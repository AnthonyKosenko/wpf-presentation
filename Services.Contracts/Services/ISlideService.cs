﻿using Services.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Contracts.Services
{
    public interface ISlideService
    {
        Task AddSlideToPresentation(int presentationId, SlideDto slide);

        void AddAllContentToPresentation(int presentationid);

        Task UpdateSlide(int slideId, MediaDto media, TimeSpan? duration = null);
    }
}
