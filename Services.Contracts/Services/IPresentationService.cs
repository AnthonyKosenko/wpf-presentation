﻿using Services.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Contracts.Services
{
    public interface IPresentationService
    {
        Task<IEnumerable<PresentationDto>> GetPresentations();

        Task<PresentationDto> GetPresentationById(int id);

        Task AddPresentation(string name);
    }
}
