﻿using Model;
using Services.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Contracts.Mappers
{
    public interface ISlideMapper: IMapper<Slide , SlideDto>
    {
    }
}
