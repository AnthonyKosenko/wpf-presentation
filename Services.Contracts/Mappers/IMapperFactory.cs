﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Contracts.Mappers
{
    public interface IMapperFactory
    {
        T GetMapper<T>()
           where T : IMapper;
    }


    public interface IMapper
    { }

    public interface IMapper<S, T> : IMapper
       where S : class
       where T : class
    {
        TDestination Map<TSource, TDestination>(TSource source, Action<TSource, TDestination> extra = null, TDestination destination = null)
            where TSource : class
            where TDestination : class;

        IEnumerable<TDestination> MapCollection<TSource, TDestination>(IEnumerable<TSource> source, Action<TSource, TDestination> extra = null);

        IEnumerable<S> MapCollectionFromModel(IEnumerable<T> source, Action<T, S> extra = null);
        IEnumerable<T> MapCollectionToModel(IEnumerable<S> source, Action<S, T> extra = null);
        IEnumerable<T> MapCollectionToModel(IEnumerable source, Action<S, T> extra = null);
        S MapFromModel(T source, Action<T, S> extra = null, S destination = null);
        T MapToModel(S source, Action<S, T> extra = null, T destination = null);
    }
}
