﻿using Services.Contracts.Mappers;
using Services.Contracts.Services;
using Services.Mappers;
using Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;

namespace Services
{
    public class ContainerConfiguration
    {
        public static void RegisterTypes(IUnityContainer container)
        {
            Data.ContainerConfiguration.RegisterTypes(container);

            container.RegisterType<IMapperFactory, MapperFactory>();
            container.RegisterType<IPresentationMapper, PresentationMapper>();
            container.RegisterType<IPresentationService, PresentationService>();
            container.RegisterType<ISlideService, SlideService>();
        }
    }
}
