﻿using Data.Contracts.Repositories;
using log4net;
using Model;
using Services.Contracts.Mappers;
using Services.Contracts.Models;
using Services.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Services
{
    public class SlideService : AbstractService, ISlideService
    {
        public ISlideRepository SlideRepository { get; set; }

        public SlideService(IMapperFactory mapperFactory, ILog log, ISlideRepository slideRepository) : base(mapperFactory, log)
        {
            SlideRepository = slideRepository;
        }
        
        public async Task AddSlideToPresentation(int presentationId, SlideDto slide)
        {
            var lastSlide = await SlideRepository.GetLastSlide(presentationId);

            var newSlide = new Slide
            {
                PresentationId = presentationId,
                Number = lastSlide == null ? 1 : lastSlide.Number++,
                Duration = slide.Duration
            };

            SlideRepository.AddSlide(newSlide);
            await SlideRepository.SaveAsync();
        }

        public void AddAllContentToPresentation(int presentationid)
        {
            SlideRepository.GetPresentationContentById(presentationid);
        }

        public async Task UpdateSlide(int slideId, MediaDto media, TimeSpan? duration = null)
        {
            var slide = await SlideRepository.GetSlideById(slideId);

            if (slide == null)
            {
                throw new ArgumentNullException(nameof(slide));
            }

            if (duration.HasValue)
            {
                slide.Duration = duration.Value;
            }

            slide.Media = new Media
            {
                FirstFilaName = media.FirstFileName,
                SecondFileName = media.SecondFileName,
                Type = media.Type
            };

            SlideRepository.Update(slide);
            await SlideRepository.SaveAsync();
        }
    }
}
