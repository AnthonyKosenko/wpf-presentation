﻿using Data;
using Data.Contracts.Repositories;
using log4net;
using Services.Contracts.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Services
{
    public abstract class AbstractService
    {
        public AbstractService(IMapperFactory mapperFactory, ILog log)
        {
            Log = log;
            MapperFactory = mapperFactory;
        }

        public ILog Log { get; private set; }

        public IMapperFactory MapperFactory { get; private set; }
    }
}
