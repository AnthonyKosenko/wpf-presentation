﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Contracts.Repositories;
using log4net;
using Model;
using Services.Contracts.Mappers;
using Services.Contracts.Models;
using Services.Contracts.Services;

namespace Services.Services
{
    public class PresentationService : AbstractService, IPresentationService
    {
        public IPresentationRepository PresentationRepository { get; private set; }
        public PresentationService(IMapperFactory mapperFactory, ILog log, IPresentationRepository presentationRepository) : base(mapperFactory, log)
        {
            PresentationRepository = presentationRepository;
        }

        public async Task<IEnumerable<PresentationDto>> GetPresentations()
        {
            var presentations = await PresentationRepository.GetPresentations();

            var presentationDtos = MapperFactory.GetMapper<IPresentationMapper>().MapCollectionToModel(presentations);

            return presentationDtos;
        }

        public async Task<PresentationDto> GetPresentationById(int id)
        {
            var presentation =  await PresentationRepository.GetPresentationById(id);

            var presentationDto = MapperFactory.GetMapper<IPresentationMapper>().MapToModel(presentation);

            return presentationDto;
        }

        public async Task AddPresentation(string name)
        {
            var presentation = new Presentation
            {
                Name = name,
                Slides = new List<Slide>()
            };

            PresentationRepository.AddPresentation(presentation);
            await PresentationRepository.SaveAsync();
        }
    }
}
