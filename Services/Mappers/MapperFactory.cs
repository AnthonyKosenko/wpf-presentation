﻿using Services.Contracts.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;

namespace Services.Mappers
{
    public class MapperFactory : IMapperFactory
    {
        public MapperFactory(IUnityContainer container)
        {
            Container = container;
        }

        public IUnityContainer Container { get; private set; }
        public T GetMapper<T>() where T : IMapper
        {
            return Container.Resolve<T>();
        }
    }
}
