﻿using AutoMapper;
using Model;
using Services.Contracts.Mappers;
using Services.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Mappers
{
    public class PresentationMapper : AbstractMapper<Presentation, PresentationDto>, IPresentationMapper
    {
        protected override AutoMapper.IMapper Configure()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Media, MediaDto>()
                    .ForMember(x => x.Id, y => y.MapFrom(z => z.Id))
                    .ForMember(x => x.FirstFileName, y => y.MapFrom(z => z.FirstFilaName))
                    .ForMember(x => x.SecondFileName, y => y.MapFrom(z => z.SecondFileName))
                    .ForMember(x => x.Type, y => y.MapFrom(z => z.Type));
                
                cfg.CreateMap<Slide, SlideDto>()
                   .ForMember(x => x.Id, y => y.MapFrom(z => z.Id))
                   .ForMember(x => x.Number, y => y.MapFrom(z => z.Number))
                   .ForMember(x => x.Media, y => y.MapFrom(z => z.Media));

                cfg.CreateMap<Presentation, PresentationDto>()
                    .ForMember(x => x.Id, y => y.MapFrom(z => z.Id))
                    .ForMember(x => x.Name, y => y.MapFrom(z => z.Name))
                    .ForMember(x => x.Slides, y => y.MapFrom(z => z.Slides));

                cfg.CreateMap<MediaDto, Media>();
                cfg.CreateMap<SlideDto, Slide>();
                cfg.CreateMap<PresentationDto, Presentation>();
            });

            return config.CreateMapper();
        }
    }
}
