﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Contracts.Repositories
{
    public interface ISlideRepository: IRepository<Slide>
    {
        void AddSlide(Slide slide);

        Task<Slide> GetLastSlide(int presentationId);

        Task<Slide> GetSlideById(int slideId);

        List<Slide> GetPresentationContentById(int presentationId);
    } 
}
