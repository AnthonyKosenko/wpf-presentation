﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Contracts.Repositories
{
    public interface IPresentationRepository: IRepository<Presentation>
    {
        Task<Presentation> GetPresentationById(int id);

        Task<IEnumerable<Presentation>> GetPresentations();

        void AddPresentation(Presentation presentation);
    }
}
