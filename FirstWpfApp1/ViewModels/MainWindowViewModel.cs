﻿using Services.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstWpfApp1.ViewModels
{
    public class MainWindowViewModel
    {
        public IPresentationService PresentationService { get; private set; }
        public ISlideService SlideService { get; private set; }

        public MainWindowViewModel(IPresentationService presentationService, ISlideService slideService)
        {
            PresentationService = presentationService;
            SlideService = slideService;
        }
    }
}
