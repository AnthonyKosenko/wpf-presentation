﻿using FirstWpfApp1.Helpers;
using FirstWpfApp1.Models;
using FirstWpfApp1.ViewModels;
using Microsoft.Win32;
using Services.Contracts.Models;
using Services.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Unity;

namespace FirstWpfApp1
{
    public partial class MainWindow : Window
    {
        [Dependency]
        public IPresentationService PresentationService { get; set; }

        [Dependency]
        public ISlideService SlideService { get; set; }

        [Dependency]
        public IFileHelper FileHelper { get; set; }


        private PresentationModel _selectedPresentation;

        public MainWindow()
        {
            InitializeComponent();
        }

        private async void getPresentations_Loaded(object sender, RoutedEventArgs e)
        {
            await GetPresentations();
        }

        private async Task GetPresentations()
        {
            var presentationDtos = await PresentationService.GetPresentations();

            var presentationInfoModels = presentationDtos
                .Select(pr => new PresentationInfoModel
                {
                    Id = pr.Id,
                    Name = pr.Name
                })
                .ToList();

            presentationList.ItemsSource = presentationInfoModels;
        }

        private async void addPresentation_Click(object sender, RoutedEventArgs e)
        {
            var presentationInfoModels = presentationList.ItemsSource as IEnumerable<PresentationInfoModel>;

            var presentationName = "Presentation " + (presentationInfoModels.Count() + 1);

            await PresentationService.AddPresentation(presentationName);

            await GetPresentations();
        }

        private async void selectPresentation_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SlideList.ItemsSource = null;
            PreviousSlide.Background = null;
            PreviousSlideImage.Source = null;
            PreviousSlideVideo.Source = null;

            CurrentSlide.Background = null;
            CurrentSlideImage.Source = null;
            CurrentSlideVideo.Source = null;

            PresentationProgress.Visibility = Visibility.Hidden;

            var presentationInfoModel = e.AddedItems[0] as PresentationInfoModel;

            await GetPresentation(presentationInfoModel.Id);

            PresentationGrid.Visibility = Visibility.Visible;
        }

        private void putInCurrentSlide_Click(object sender, MouseButtonEventArgs e)
        {
            SlideInfoModel slide = null;

            if (e.Source is Grid)
            {
                var grid = e.Source as Grid;
                slide = grid.DataContext as SlideInfoModel;
            }
            else if (e.Source is Image)
            {
                var grid = e.Source as Image;
                slide = grid.DataContext as SlideInfoModel;
            }

            CurrentSlide.DataContext = slide;

            PutMediaSource(slide.FullPath, CurrentSlideImage, CurrentSlideVideo, CurrentSlide);

            var slides = SlideList.ItemsSource as IEnumerable<SlideInfoModel>;

            SetProgressBarValue();

            var previousSlide = slides.FirstOrDefault(x => x.Number == (slide.Number - 1));

            if (previousSlide != null)
            {
                PreviousSlide.DataContext = previousSlide;

                PutMediaSource(previousSlide.FullPath, PreviousSlideImage, PreviousSlideVideo, PreviousSlide);
            }
            else
            {
                PreviousSlide.DataContext = null;
                PreviousSlide.Background = null;
                PreviousSlideImage.Source = null;
                PreviousSlideVideo.Source = null;
            }
        }

        private void PutMediaSource(string fullPath, Image image, MediaElement video, Grid grid)
        {
            image.Source = null;
            video.Source = null;
            grid.Background = Brushes.Gray;

            if (fullPath.EndsWith(".bmp") || fullPath.EndsWith(".jpg"))
            {
                image.Visibility = Visibility.Visible;
                video.Visibility = Visibility.Hidden;
                image.Source = new BitmapImage(new Uri(fullPath));
            }
            else if (fullPath.EndsWith(".mp4") || fullPath.EndsWith(".avi"))
            {
                var testForThumbnail = ImportMedia(fullPath, 10, 10);
                image.Visibility = Visibility.Hidden;
                video.Visibility = Visibility.Visible;
                video.Source = new Uri(fullPath);
            }
            else if (fullPath.EndsWith(".gif"))
            {
            }
        }

        

        private BitmapImage ImportMedia(string filePath, int waitTime, int position)
        {
            MediaPlayer player = new MediaPlayer { Volume = 0, ScrubbingEnabled = true };

            player.Open(new Uri(filePath));
            player.Position = TimeSpan.FromSeconds(position);

            RenderTargetBitmap rtb = new RenderTargetBitmap(120, 90, 96, 96, PixelFormats.Pbgra32);
            var bitmapImage = new BitmapImage();
            var bitmapEncoder = new PngBitmapEncoder();
            bitmapEncoder.Frames.Add(BitmapFrame.Create(rtb));

            DrawingVisual dv = new DrawingVisual();

            using (DrawingContext dc = dv.RenderOpen())
            {
                dc.DrawVideo(player, new Rect(0, 0, 120, 90));
            }

            
            rtb.Render(dv);

            using (var stream = new MemoryStream())
            {
                bitmapEncoder.Save(stream);
                stream.Seek(0, SeekOrigin.Begin);

                bitmapImage.BeginInit();
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.StreamSource = stream;
                bitmapImage.EndInit();
                return bitmapImage;
            }
        }

        public static byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }

                var ereree = ms;

                return ms.ToArray();
            }
        }

        public async void addSlide_Click(object sender, RoutedEventArgs e)
        {
            var slideDto = new SlideDto
            {
                Duration = TimeSpan.FromSeconds(1)
            };

            await SlideService.AddSlideToPresentation(_selectedPresentation.Id, slideDto);
            await GetPresentation(_selectedPresentation.Id);
        }

        public async void updateSlide_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();

            ofd.Filter = "Image Files(*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF|Video Files(*.MP4; *.AVI)|*.MP4; *.AVI|All files (*.*)|*.*";

            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                var fileInfo = new FileInfo(ofd.FileName);

                try
                {
                    var media = new MediaDto
                    {
                        FirstFileName = fileInfo.Name,
                        SecondFileName = Guid.NewGuid(),
                        Type = FileHelper.GetFileType(fileInfo.FullName)
                    };

                    TimeSpan? duration = null;

                    if (media.Type == Model.Enumerators.MediaType.Video)
                    {
                        duration = FileHelper.GetVideoDuration(fileInfo.FullName);
                    }
                        
                    try
                    {
                        FileHelper.SaveFile(fileInfo.FullName, media.SecondFileName);
                    }
                    catch (Exception)
                    {
                        throw;
                    }

                    var currentSlide = CurrentSlide.DataContext as SlideInfoModel;

                    await SlideService.UpdateSlide(currentSlide.Id, media, duration);
                    var slides = await GetPresentation(_selectedPresentation.Id);

                    currentSlide = slides.FirstOrDefault(x => x.Number == currentSlide.Number);

                    if (currentSlide != null)
                    {
                        PutMediaSource(currentSlide.FullPath, CurrentSlideImage, CurrentSlideVideo, CurrentSlide);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public async void savePresentation_Click(object sender, RoutedEventArgs e)
        {

            await GetPresentation(_selectedPresentation.Id);
            var uploadPath = "Presenation";
            if (!Directory.Exists(uploadPath))
                Directory.CreateDirectory(uploadPath);

            SlideService.AddAllContentToPresentation(_selectedPresentation.Id);
            FileHelper.SavePresentation(_selectedPresentation.Id, Guid.NewGuid());

        }

        private async Task<IEnumerable<SlideInfoModel>> GetPresentation(int presentationId)
        {
            var presentationDto = await PresentationService.GetPresentationById(presentationId);

            var presentationModel = new PresentationModel
            {
                Id = presentationDto.Id,
                Name = presentationDto.Name,
                Slides = presentationDto.Slides.Select(sl => new PresentationModel.SlideModel
                {
                    Id = sl.Id,
                    Number = sl.Number,
                    Duration = sl.Duration,
                    File = sl.Media != null ? new PresentationModel.SlideModel.FileModel
                    {
                        FirstFileName = sl.Media.FirstFileName,
                        SecondFileName = sl.Media.SecondFileName,
                        Id = sl.Media.Id,
                        Type = sl.Media.Type
                    } : null
                })
                .ToList()
            };

            var slides = presentationModel.Slides
                .Select(s => new SlideInfoModel
                {
                    Id = s.Id,
                    Duration = s.Duration,
                    FullPath = s.File != null ? FileHelper.GetFileByName(s.File.SecondFileName) : string.Empty,
                    FileName = s.File != null ? s.File.FirstFileName : string.Empty,
                    Number = s.Number,
                    Type = s.File?.Type
                })
                .OrderBy(ord => ord.Number)
                .ToList();

            SlideList.ItemsSource = slides;
            _selectedPresentation = presentationModel;

            return slides;
        }

        DispatcherTimer timer;
        IEnumerator<SlideInfoModel> slideEnum;

        private void startPresentation_Click(object sender, MouseButtonEventArgs e)
        {
            var allSlides = SlideList.ItemsSource as IEnumerable<SlideInfoModel>;
            var currentSlide = CurrentSlide.DataContext as SlideInfoModel;

            var slides = new List<SlideInfoModel>();

            if (currentSlide != null)
            {
                slides = allSlides.Where(x => x.Number >= currentSlide.Number).ToList();
            }

            slideEnum = slides.GetEnumerator();
            slideEnum.MoveNext();

            startButton.Visibility = Visibility.Hidden;
            stopButton.Visibility = Visibility.Visible;

            timer = new DispatcherTimer { Interval = new TimeSpan(0, 0, 1) };
            timer.Tick += playPresentation_Tick;
            timer.Start();
        }

        double nextProgressBarValue = 0;
        bool isCreased = false;
        bool isPlayed = false;
        private void playPresentation_Tick(object sender, object e)
        {
            if (slideEnum.Current != null)
            {
                CurrentSlide.DataContext = slideEnum.Current;

                if (slideEnum.Current.Type.HasValue)
                {
                    if (slideEnum.Current.Type.Value == Model.Enumerators.MediaType.Image)
                    {
                        CurrentSlideImage.Visibility = Visibility.Visible;
                        CurrentSlideVideo.Visibility = Visibility.Hidden;

                        if (!isPlayed)
                        {
                            CurrentSlideImage.Source = new BitmapImage(new Uri(slideEnum.Current.FullPath));
                            isPlayed = true;
                        }
                    }
                    else if (slideEnum.Current.Type.Value == Model.Enumerators.MediaType.Video)
                    {
                        CurrentSlideImage.Visibility = Visibility.Hidden;
                        CurrentSlideVideo.Visibility = Visibility.Visible;

                        if (!isPlayed)
                        {
                            CurrentSlideVideo.Source = new Uri(slideEnum.Current.FullPath);

                            CurrentSlideVideo.Play();
                            isPlayed = true;
                        }
                    }
                }
                else
                {
                    CurrentSlideVideo.Source = null;
                    CurrentSlideImage.Source = null;

                }

                if (!isCreased)
                {
                    nextProgressBarValue = ProgressBar.Value + slideEnum.Current.Duration.TotalSeconds;
                    isCreased = true;
                }

                if (ProgressBar.Value < nextProgressBarValue)
                {
                    ProgressBar.Value++;
                }
                else
                {
                    isCreased = false;
                    isPlayed = false;

                    if (slideEnum.Current.Type.HasValue && slideEnum.Current.Type.Value == Model.Enumerators.MediaType.Video)
                    {
                        CurrentSlideVideo.Stop();
                    }

                    if (!slideEnum.MoveNext())
                    {
                        timer.Stop();

                        isCreased = false;
                        isPlayed = false;

                        nextProgressBarValue = 0;

                        SetProgressBarValue();

                        startButton.Visibility = Visibility.Visible;
                        stopButton.Visibility = Visibility.Hidden;
                    }
                }
            }
            else
            {
                timer.Stop();

                isCreased = false;
                isPlayed = false;

                nextProgressBarValue = 0;

                SetProgressBarValue();

                startButton.Visibility = Visibility.Visible;
                stopButton.Visibility = Visibility.Hidden;
            }
        }

        private void stopPresentation_Click(object sender, MouseButtonEventArgs e)
        {
            var currentSlide = CurrentSlide.DataContext as SlideInfoModel;

            if (currentSlide.Type.HasValue && currentSlide.Type.Value == Model.Enumerators.MediaType.Video)
            {
                CurrentSlideVideo.Stop();
            }

            timer.Stop();

            nextProgressBarValue = 0;

            SetProgressBarValue();

            startButton.Visibility = Visibility.Visible;
            stopButton.Visibility = Visibility.Hidden;
        }

        private void SetProgressBarValue()
        {
            var allSlides = SlideList.ItemsSource as IEnumerable<SlideInfoModel>;
            var currentSlide = CurrentSlide.DataContext as SlideInfoModel;

            ProgressBar.Value = allSlides.Where(x => x.Number < currentSlide.Number).Select(s => s.Duration.TotalSeconds).Sum();
            ProgressBar.Maximum = allSlides.Select(s => s.Duration.TotalSeconds).Sum();

            PutMediaSource(currentSlide.FullPath, CurrentSlideImage, CurrentSlideVideo, CurrentSlide);

            var previousSlide = allSlides.FirstOrDefault(z => z.Number == currentSlide.Number - 1);

            if (previousSlide != null)
            {
                PutMediaSource(previousSlide.FullPath, PreviousSlideImage, PreviousSlideVideo, PreviousSlide);
            }

            PresentationProgress.Visibility = Visibility.Visible;
        }
    }
}
