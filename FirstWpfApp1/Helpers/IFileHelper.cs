﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstWpfApp1.Helpers
{
    public interface IFileHelper
    {
        string GetFileByName(Guid fileId);

        void SaveFile(string filePath, Guid newFileName);

        void SavePresentation(int presentationId, Guid NewPresentation);

        TimeSpan GetVideoDuration(string filePath);

        Model.Enumerators.MediaType GetFileType(string filePath);
    }
}
