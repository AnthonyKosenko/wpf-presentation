﻿using Microsoft.WindowsAPICodePack.Shell;
using Microsoft.WindowsAPICodePack.Shell.PropertySystem;
using Model.Enumerators;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using System.Threading.Tasks;
using System.Windows;

namespace FirstWpfApp1.Helpers
{
    public class FileHelper : IFileHelper
    {
        private const string DIRECTORY_NAME = "Temp";
        private const string DIRECTORY_NAME2 = "Pres";



        private readonly string DIRECTORY_PATH;
       // private readonly string DIRECTORY_PATH2;

        public FileHelper()
        {
            string root = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

            DIRECTORY_PATH = Path.Combine(root, DIRECTORY_NAME);

            if (!Directory.Exists(DIRECTORY_PATH))
            {
                Directory.CreateDirectory(DIRECTORY_PATH);
            }
        }

        public string GetFileByName(Guid fileId)
        {
            var files = Directory.GetFiles(DIRECTORY_PATH, fileId.ToString() + ".*");

            return files.FirstOrDefault();
        }

        public MediaType GetFileType(string filePath)
        {
            if (filePath.EndsWith(".bmp") || filePath.EndsWith(".jpg"))
            {
                return MediaType.Image;
            }
            else if (filePath.EndsWith(".mp4") || filePath.EndsWith(".avi"))
            {
                return MediaType.Video;
            }
            else if (filePath.EndsWith(".gif"))
            {
                return MediaType.Animation;
            }
            else
            {
                throw new Exception();
            }
        }

        public TimeSpan GetVideoDuration(string filePath)
        {
            using (var shell = ShellObject.FromParsingName(filePath))
            {
                IShellProperty property = shell.Properties.System.Media.Duration;
                var ticks = (ulong)property.ValueAsObject;

                return TimeSpan.FromTicks((long)ticks);
            }
        }

        public void SaveFile(string filePath, Guid newFileName)
        {
            var extension = Path.GetExtension(filePath);

            var copyFilePath = Path.Combine(DIRECTORY_PATH, newFileName + extension);

            File.Copy(filePath, copyFilePath);
        }

        public void SavePresentation (int presentationId, Guid NewPresentation)
        {
            string path = Path.Combine(presentationId.ToString(), DIRECTORY_NAME);

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
                MessageBox.Show("New file created");
            }

            //var completePresentration = presentationId.ToString() + " presentation";

            //new FileInfo(completePresentration).Directory.Create();

            var extension = Path.GetExtension(presentationId.ToString());

            var copyFilePath = Path.Combine("C:\\Users\\USER\\source\\repos\\FirstWpfApp1\\FirstWpfApp1\\bin\\Debug\\Temp", NewPresentation.ToString() + extension);

            File.Copy(presentationId.ToString(), copyFilePath);
        }
    }
}
