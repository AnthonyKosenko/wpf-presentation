﻿using FirstWpfApp1.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Unity;
using Unity.Injection;

namespace FirstWpfApp1
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            //base.OnStartup(e);

            IUnityContainer container = new UnityContainer();

            Infrastructure.ContainerConfiguration.RegisterTypes(container);

            var window = container.Resolve<MainWindow>();
            window.Show();
        }
    }
}
