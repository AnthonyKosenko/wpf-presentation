﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace FirstWpfApp1.Models
{
    public class SlideInfoModel
    {
        public int Id { get; set; }

        public int Number { get; set; }

        public TimeSpan Duration { get; set; }

        public string FullPath { get; set; }

        public string FileName { get; set; }

        public Model.Enumerators.MediaType? Type { get; set; }
    }
}
