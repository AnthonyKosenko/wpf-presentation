﻿using Model.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstWpfApp1.Models
{
    public class PresentationModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public IEnumerable<SlideModel> Slides { get; set; }

        public class SlideModel
        {
            public int Id { get; set; }

            public int Number { get; set; }

            public TimeSpan Duration { get; set; }
            
            public FileModel File { get; set; }
            
            public class FileModel
            {
                public int Id { get; set; }

                public string FirstFileName { get; set; }

                public Guid SecondFileName { get; set; }

                public string FullPath { get; set; }

                public MediaType Type { get; set; }
            }
        }
    }
}
