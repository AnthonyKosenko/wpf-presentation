﻿using FirstWpfApp1.Helpers;
using FirstWpfApp1.Infrastructure.Mappers;
using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;

namespace FirstWpfApp1.Infrastructure
{
    public class ContainerConfiguration
    {
        public static void RegisterTypes(IUnityContainer container)
        {
            Services.ContainerConfiguration.RegisterTypes(container);

            var log = LogManager.GetLogger("Logger");
            XmlConfigurator.Configure();
            container.RegisterInstance(log);

            container.RegisterType<IModelMapperFactory, ModelMapperFactory>();

            container.RegisterType<IFileHelper, FileHelper>();
        }
    }
}
