﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Enumerators
{
    public enum MediaType
    {
        Image = 1,
        Video = 2,
        Animation = 3
    }
}
