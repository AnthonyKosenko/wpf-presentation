﻿using Model.Enumerators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Media
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string FirstFilaName { get; set; }

        [Required]
        public Guid SecondFileName { get; set; }

        [Required]
        public MediaType Type { get; set; }
    }
}
