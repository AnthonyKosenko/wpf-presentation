﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Slide
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public int PresentationId { get; set; }

        [Required]
        public int Number { get; set; }

        public int? MediaId { get; set; }

        [Required]
        public TimeSpan Duration { get; set; }

        public virtual Presentation Presentation { get; set; }

        public virtual Media Media { get; set; }
    }
}
