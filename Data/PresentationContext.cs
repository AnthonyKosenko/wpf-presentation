﻿using Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class PresentationContext: DbContext
    {
        public PresentationContext(): base("DefaultConnection") { }

        public DbSet<Presentation> Presentations { get; set; }

        public DbSet<Slide> Slides { get; set; }

        public DbSet<Media> Medias { get; set; }
    }
}
