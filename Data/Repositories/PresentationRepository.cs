﻿using Data.Contracts.Repositories;
using Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class PresentationRepository : AbstractRepository<Presentation>, IPresentationRepository
    {
        public PresentationRepository(PresentationContext context): base(context) { }

        public void AddPresentation(Presentation presentation)
        {
            Context.Presentations.Add(presentation);
        }

        public async Task<Presentation> GetPresentationById(int id)
        {
            return await Context.Presentations.Include(inc => inc.Slides).FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<IEnumerable<Presentation>> GetPresentations()
        {
            return await Context.Presentations.ToListAsync();
        }
    }
}
