﻿using Data.Contracts.Repositories;
using Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class SlideRepository : AbstractRepository<Slide>, ISlideRepository
    {
        public SlideRepository(PresentationContext context) : base(context)
        {
        }

        public void AddSlide(Slide slide)
        {
            Context.Slides.Add(slide);
        }
        
        public async Task<Slide> GetLastSlide(int presentationId)
        {
            return await Context.Slides
                .Where(x => x.PresentationId == presentationId)
                .OrderByDescending(ord => ord.Number)
                .FirstOrDefaultAsync();
        }

        public List<Slide> GetPresentationContentById(int id)
        {
            return Context.Slides.Where(x => x.PresentationId == id).ToList();
        }

        public async Task<Slide> GetSlideById(int slideId)
        {
            return await Context.Slides
                .FirstOrDefaultAsync(x => x.Id == slideId);
        }
    }
}
