﻿using Data.Contracts.Repositories;
using Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;

namespace Data
{
    public class ContainerConfiguration
    {
        public static void RegisterTypes(IUnityContainer container)
        {
            container.RegisterSingleton<PresentationContext>();

            container.RegisterType<IPresentationRepository, PresentationRepository>();
            container.RegisterType<ISlideRepository, SlideRepository>();
        }
    }
}
