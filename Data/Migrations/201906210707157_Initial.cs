namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Images",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        ImageId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Presentations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Slides",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PresentationId = c.Int(nullable: false),
                        Number = c.Int(nullable: false),
                        ImageId = c.Int(),
                        VideoId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Images", t => t.ImageId)
                .ForeignKey("dbo.Presentations", t => t.PresentationId, cascadeDelete: true)
                .ForeignKey("dbo.Videos", t => t.VideoId)
                .Index(t => t.PresentationId)
                .Index(t => t.ImageId)
                .Index(t => t.VideoId);
            
            CreateTable(
                "dbo.Videos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        VideoId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Slides", "VideoId", "dbo.Videos");
            DropForeignKey("dbo.Slides", "PresentationId", "dbo.Presentations");
            DropForeignKey("dbo.Slides", "ImageId", "dbo.Images");
            DropIndex("dbo.Slides", new[] { "VideoId" });
            DropIndex("dbo.Slides", new[] { "ImageId" });
            DropIndex("dbo.Slides", new[] { "PresentationId" });
            DropTable("dbo.Videos");
            DropTable("dbo.Slides");
            DropTable("dbo.Presentations");
            DropTable("dbo.Images");
        }
    }
}
