namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeToMedia : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Slides", "ImageId", "dbo.Images");
            DropForeignKey("dbo.Slides", "VideoId", "dbo.Videos");
            DropIndex("dbo.Slides", new[] { "ImageId" });
            DropIndex("dbo.Slides", new[] { "VideoId" });
            CreateTable(
                "dbo.Media",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstFilaName = c.String(nullable: false, maxLength: 100),
                        SecondFileName = c.Guid(nullable: false),
                        Type = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Slides", "MediaId", c => c.Int());
            CreateIndex("dbo.Slides", "MediaId");
            AddForeignKey("dbo.Slides", "MediaId", "dbo.Media", "Id");
            DropColumn("dbo.Slides", "ImageId");
            DropColumn("dbo.Slides", "VideoId");
            DropTable("dbo.Images");
            DropTable("dbo.Videos");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Videos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        VideoId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Images",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        ImageId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Slides", "VideoId", c => c.Int());
            AddColumn("dbo.Slides", "ImageId", c => c.Int());
            DropForeignKey("dbo.Slides", "MediaId", "dbo.Media");
            DropIndex("dbo.Slides", new[] { "MediaId" });
            DropColumn("dbo.Slides", "MediaId");
            DropTable("dbo.Media");
            CreateIndex("dbo.Slides", "VideoId");
            CreateIndex("dbo.Slides", "ImageId");
            AddForeignKey("dbo.Slides", "VideoId", "dbo.Videos", "Id");
            AddForeignKey("dbo.Slides", "ImageId", "dbo.Images", "Id");
        }
    }
}
