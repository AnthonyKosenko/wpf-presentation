namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SlideDuration : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Slides", "Duration", c => c.Time(nullable: false, precision: 7));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Slides", "Duration");
        }
    }
}
